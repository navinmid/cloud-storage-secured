/**
 * UploadsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

// var Uploads = require('../models/Uploads');
var path = require('path');

module.exports = {
  
    create: function (req, res) {
        req.file('file').upload({
            dirname: path.resolve(sails.config.appPath, 'assets/userfiles')
        },function (err, uploadedFiles){
            if (err) return res.serverError(err);
            let data = { name: uploadedFiles[0].filename, file: path.basename(uploadedFiles[0].fd), owner: req.me.id}
            UserFile.create(data).fetch().exec(function (err, file) {
                if (err) return (err);

                FileActionLog.create({file: file.id, actionType: 'create', doneBy: req.me.id}).fetch().exec(function (err, fileActionLog) {});
                
                return res.json(file);
            })
        });
    },

    list: function (req, res) {
        (async () => {
            var files = await UserFile.find({owner: req.me.id});
            return res.json(files)
        })()
    },

    share: function (req, res) {
        let data = { file: req.body.fileId, secret: req.body.secret, sharedWith: req.body.userId, sharedBy: req.me.id}
        UserFileShare.create(data).fetch().exec(function (err, fileShare) {
            if (err) return (err);

            FileActionLog.create({file: req.body.fileId, actionType: 'share', doneBy: req.me.id}).fetch().exec(function (err, fileActionLog) {});
            return res.json(fileShare);
        })
    },

    listSharedFiles: function (req, res) {
        (async () => {
            var sharedFiles = await UserFileShare.find({sharedWith: req.me.id}).populate('file');
            return res.json(sharedFiles);
        })()
    },

    download: function (req, res) {
        (async () => {
            FileActionLog.create({file: req.param('id'), actionType: 'download', doneBy: req.me.id}).fetch().exec(function (err, fileActionLog) {});
            var file = await UserFile.findOne({id: req.param('id')});
            res.download(path.resolve(sails.config.appPath, 'assets/userfiles') + "/" + file.file, file.name);
        })()
    },

    logs: function (req, res) {
        (async () => {
            var logs = await FileActionLog.find().populate('file').populate('doneBy');
            return res.json(logs);
        })()
    }

};

