/**
 * UploadsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

// var Uploads = require('../models/Uploads');
var path = require('path');

module.exports = {

    list: function (req, res) {
        (async () => {
            var files = await User.find({id: { '!' : [req.me.id] }});
            return res.json(files)
        })()
    }

};

