# Access Controlled Cloud

A cloud application to store files


### Install required dependencies

Go to project folder and then run the following commands to install required dependencies

+ `npm install sails -g`
+ `npm install`


### To start application

+ `sails lift`
