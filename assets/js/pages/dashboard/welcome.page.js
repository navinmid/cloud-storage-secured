Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(value).format('MM/DD/YYYY hh:mm')
  }
});
parasails.registerPage('welcome', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    selectedFile: {},
    selectedDownloadFile: undefined,
    selectedUser: {},
    modal: '',
    files: [],
    sharedFiles: [],
    pageLoadedAt: Date.now(),
    users: [],
    secret: ''
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    // Attach any initial data from the server.
    _.extend(this, SAILS_LOCALS);
    this.refreshFiles();
  },
  mounted: async function() {
    //…
  },

  //  ╦  ╦╦╦═╗╔╦╗╦ ╦╔═╗╦    ╔═╗╔═╗╔═╗╔═╗╔═╗
  //  ╚╗╔╝║╠╦╝ ║ ║ ║╠═╣║    ╠═╝╠═╣║ ╦║╣ ╚═╗
  //   ╚╝ ╩╩╚═ ╩ ╚═╝╩ ╩╩═╝  ╩  ╩ ╩╚═╝╚═╝╚═╝
  // Configure deep-linking (aka client-side routing)
  virtualPagesRegExp: /^\/welcome\/?([^\/]+)?\/?/,
  afterNavigate: async function(virtualPageSlug){
    // `virtualPageSlug` is determined by the regular expression above, which
    // corresponds with `:unused?` in the server-side route for this page.
    switch (virtualPageSlug) {
      case 'hello':
        this.modal = 'example';
        break;
      default:
        this.modal = '';
    }
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {

    clickShareModal: async function(file) {
      this.selectedFile = file;
      this.goto('/welcome/hello');
      // Or, without deep links, instead do:
      // ```
      // this.modal = 'example';
      // ```
    },

    downloadFile: async function(file) {
      this.goto('/welcome');
      this.goto('/api/v1/download/' + file.id);
    },

    downloadSharedFile: async function(shared) {
      this.selectedDownloadFile = shared;
      this.secret = "";
      this.goto('/welcome/hello');
    },

    closeExampleModal: async function() {
      this.goto('/welcome');
      this.selectedDownloadFile = undefined;
      // Or, without deep links, instead do:
      // ```
      // this.modal = '';
      // ```
    },

    refreshFiles: async function() {
      var _this = this;
      $.get( "/api/v1/files", function( data ) {
        _this.files = data;
      });
      $.get( "/api/v1/users", function( data ) {
        _this.users = data;
      });
      $.get( "/api/v1/shared", function( data ) {
        _this.sharedFiles = data;
      });
    },

    uploadFile: async function() {
      var _this = this;
      if($('#file')[0].files[0] === undefined) {
        return;
      }
      var formData = new FormData();
      formData.append('file', $('#file')[0].files[0]);
      $.ajax({
         url : '/api/v1/files',
         type : 'PUT',
         data : formData,
         processData: false,  // tell jQuery not to process the data
         contentType: false,  // tell jQuery not to set contentType
         success : function(data) {
          _this.refreshFiles();
          $( "#file" ).val("");
         }
      });
    },

    shareFile: async function(file, user, secret) {
      if(!secret) {
        return;
      }
      var _this = this;
      $.ajax({
        url : '/api/v1/share',
        type : 'PUT',
        data : JSON.stringify({
          userId: user.id,
          fileId: file.id,
          secret: secret
        }),
        contentType: 'application/json',  // tell jQuery not to set contentType
        success : function(data) {
          _this.goto('/welcome');
        }
     });
    },

    isDownloadDisabled: function(){
      console.log("Yeah")
    	return !(this.secret && this.secret === this.selectedDownloadFile.secret);
    }

  }
});
